# Gabe N.
# Quiz

# Score
score = 0

# Input
number = int(input("What is 150 + 150?"))

# Correct output
if number == 300:
    print("Correct!")
    score = score + 1

# Wrong output
else:
    print("Wrong!")

# Question
print("What is the closest planet to the Sun?")
print("a = Venus")
print("b = Mercury")
print("c = Saturn")

# Input
planet = input("Answer =")

# Correct output
if planet.lower() == "b":
    print("Correct!")
    score = score + 1

# Wrong output
else:
    print("Wrong!")

# Input
text_1 = input("Who lives in a pineapple under the sea?")

# Correct output
if text_1.lower() == "Spongebob Squarepants":
    print("Correct!")
    score = score + 1

# Wrong output
else:
    print("Wrong!")

# Questions
print("Who is the main antagonist in the Harry Potter series?")
print("a = Hagrid")
print("b = Ron")
print("c = Voldemort")

# Input
q_4 = input("Answer = ")

# Correct output
if q_4.lower() == "c":
    print("Correct!")
    score = score + 1

# Wrong output
else:
    print("Wrong")

# Input
q_5 = input("What is the square root of 9?")

# Correct output
if q_5.lower() == "3":
    print("Correct!")
    score = score + 1

# Wrong output
else:
    print("Wrong")

# Score output
print("Congratulations you finished with a score of", score, "/ 5")

# Percentage formula
percent = score / 5 * 100

# Percentage output
print("Percentage", percent)
