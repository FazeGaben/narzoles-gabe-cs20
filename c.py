# Settings for game
number_pipes = 10
pipe_x = 100
max_pipe_gap = 150
min_pipe_gap = 50
max_pipe_separation = 600
min_pipe_separation = 100
max_pipe_width = 50
min_pipe_width = 20
pipe_speed = 5
screen_width = 1280
screen_height = 720
# Player variables
player_colour = (0, 0, 0)
player_x = 100
player_width = 40
player_height = 40

