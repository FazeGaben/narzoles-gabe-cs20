import random
# Gabe N.
# Lab 4: Camel
# Title and introduction
print("Welcome to Temple Run!")
print("You have stolen an ancient gem to obtain unlimited power")
print("Monsters have been alerted and are chasing you down!")
print("Survive your jungle trek and outrun the monsters")
# Variables
done = False
miles_traveled = 0
thirst = 0
days_passed = 0
monster_d = 20
user_tired = 0
drinks_c = 3
# Options
while not done:
    print("A. Drink from your canteen.")
    print("B. Run ahead moderate speed.")
    print("C. Run ahead full speed.")
    print("D. Stop and rest")
    print("E. Status check.")
    print("Q. Quit")
    # Input
    input_1 = input("What do you want to do?")
    # Quit
    if input_1.lower() == "q":
        print("You quit the game")
        done = True
    # Status check
    if input_1.lower() == "e":
        print("Miles traveled:", miles_traveled)
        print("Drinks in canteen:", drinks_c)
        print("Miles away from the monsters:", monster_d)
        print("Days passed:", days_passed)
    # Stop and rest
    elif input_1.lower() == "d":
        print("You feel refreshed")
        monster_d = monster_d + random.randrange(-40, -20)
        user_tired = 0
        days_passed = days_passed + 1
    # Run ahead full speed
    elif input_1.lower() == "c":
        miles_traveled = miles_traveled + random.randrange(10, 20)
        print("Miles traveled:", miles_traveled)
        monster_d = monster_d + random.randrange(7, 14)
        thirst = thirst + 1
        user_tired = user_tired + random.randrange(1, 3)
        days_passed = days_passed + 1
    # Run ahead moderate speed
    elif input_1.lower() == "b":
        miles_traveled = miles_traveled + random.randrange(5, 12)
        thirst = thirst + 1
        user_tired = user_tired + 1
        monster_d = monster_d + random.randrange(7, 14)
        days_passed = days_passed + 1

    elif input_1.lower() == "a":
        if drinks_c <= 0:
            print("The canteen is empty")
        if drinks_c > 0:
            print("You drink from the canteen")
            drinks_c = drinks_c - 1
            thirst = 0

    if random.randrange(1, 20) == 10:
            print("You found a small pond")
            drinks_c = + 3
# Triggers
    if thirst == 4:
        print("You are thirsty")

    if thirst == 5:
        print("You start hallucinating")

    if thirst >= 6:
        print("You died of thirst!")
        done = True

    if user_tired == 6:
        print("You are worn out!")

    if user_tired == 7:
        print("Your vision is getting blurry")

    if user_tired == 8:
        print("You died of exhaustion")
        done = True

    if monster_d <= 5:
        print("You hear loud growls")

    if monster_d <= 0:
        print("You have been killed by the monsters!")
        done = True

    if miles_traveled >= 500:
        print("You escaped from the monsters")
        print("You win!")
        done = True
# Random events
    if user_tired >= 5 and random.randrange(1, 50) == 25:
        print("Your lack of sleep while running causes you to slip and fall into a pit of snakes")
        print("You are dead")
        done = True

    if random.randrange(1, 100) == 50:
        print("You found a supply drop containing a gun!")
        print("You shoot and kill the monsters!")
        print("You win!")
        done = True
    if random.randrange(1, 100) == 50:
        print("You have been ambushed by another group of monsters")
        print("You are dead!")
        done = True
    if random.randrange(1, 80) == 70:
        print("You drop your canteen while running frantically")
        drinks_c = 0
    if miles_traveled >= 250 and random.randrange(1, 100) == 17:
        print("You find a camp full of soldiers")
        print("You notify them about the monsters")
        print("They prepare an ambush and kill the monsters")
        print("You win!")
        done = True
