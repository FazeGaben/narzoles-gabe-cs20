"""
Example of turning a drawing into a function
Drawing by Paige Lekach
Simplified by Mr. Birrell
"""


import pygame
import math 

# Colours

BLACK = (0,   0,   0)
WHITE = (255, 255, 255)
GREEN = (0, 255,   0)
RED = (255,   0,   0)
BLUE = (0,   0, 255)
SKY_BLUE = (79, 200, 240)
GRASS_GREEN = (12, 145, 63)
BROWN = (153, 102, 0)
KITE_PINK = (255, 153, 255)
KITE_PURPLE = (204, 0, 204)
SUN_YELLOW = (255, 255, 102)
PEACH = (255, 255, 230)
BRIGHT_PINK = (255, 51, 153)
LIGHT_BROWN = (204, 153, 0)
BASKET_BROWN = (102, 51, 0)
BLANKET_RED = (179, 0, 0)
BUSH_GREEN = (27, 125, 29)
PI = 3.141592653

pygame.init()

size = (700, 500)
screen = pygame.display.set_mode(size)
pos = pygame.mouse.get_pos()
x = pos[0]
y = pos[1]
pygame.display.set_caption("Create-a-picture")


def person(screen, x ,y):

    # Person
    pygame.draw.ellipse(screen, PEACH, [450+x, 200+y, 45, 40])

    pygame.draw.ellipse(screen, BLACK, [455+x, 215+y, 5, 5])

    pygame.draw.arc(screen, GRASS_GREEN, [445+x, 212+y, 20, 20], PI / 2, PI, 2)

    pygame.draw.arc(screen, PEACH, [445+x, 212+y, 20, 20], 0, PI / 2, 2)

    pygame.draw.arc(screen, BLACK, [445+x, 212+y, 20, 20], 3 * PI / 2, 2 * PI, 2)

    pygame.draw.arc(screen, GRASS_GREEN, [445+x, 212+y, 20, 20], PI, 3 * PI / 2, 2)

    pygame.draw.rect(screen, PEACH, [467+x, 235+y, 10, 9])

    pygame.draw.rect(screen, BLUE, [448+x, 242+y, 50, 50])

    pygame.draw.rect(screen, BLUE, [435+x, 255+y, 17, 9])

    pygame.draw.ellipse(screen, PEACH, [425+x, 255+y, 10, 10])

    pygame.draw.rect(screen, BLUE, [435+x, 267+y, 17, 9])

    pygame.draw.ellipse(screen, PEACH, [425+x, 267+y, 10, 10])

    pygame.draw.rect(screen, BLACK, [448+x, 290+y, 50, 35])

    pygame.draw.rect(screen, BLACK, [442+x, 315+y, 20, 10])

    pygame.draw.rect(screen, SUN_YELLOW, [440+x, 198+y, 60, 10])

    pygame.draw.rect(screen, SUN_YELLOW, [452+x, 185+y, 40, 20])


def person0(screen, x ,y):

    # Person
    pygame.draw.ellipse(screen, PEACH, [450-425+x, 200-185+y, 45, 40])

    pygame.draw.ellipse(screen, BLACK, [455-425+x, 215-185+y, 5, 5])

    pygame.draw.arc(screen, GRASS_GREEN, [445-425+x, 212-185+y, 20, 20], PI / 2, PI, 2)

    pygame.draw.arc(screen, PEACH, [445-425+x, 212-185+y, 20, 20], 0, PI / 2, 2)

    pygame.draw.arc(screen, BLACK, [445-425+x, 212-185+y, 20, 20], 3 * PI / 2, 2 * PI, 2)

    pygame.draw.arc(screen, GRASS_GREEN, [445-425+x, 212-185+y, 20, 20], PI, 3 * PI / 2, 2)

    pygame.draw.rect(screen, PEACH, [467-425+x, 235-185+y, 10, 9])

    pygame.draw.rect(screen, BLUE, [448-425+x, 242-185+y, 50, 50])

    pygame.draw.rect(screen, BLUE, [435-425+x, 255-185+y, 17, 9])

    pygame.draw.ellipse(screen, PEACH, [425-425+x, 255-185+y, 10, 10])

    pygame.draw.rect(screen, BLUE, [435-425+x, 267-185+y, 17, 9])

    pygame.draw.ellipse(screen, PEACH, [425-425+x, 267-185+y, 10, 10])

    pygame.draw.rect(screen, BLACK, [448-425+x, 290-185+y, 50, 35])

    pygame.draw.rect(screen, BLACK, [442-425+x, 315-185+y, 20, 10])

    pygame.draw.rect(screen, SUN_YELLOW, [440-425+x, 198-185+y, 60, 10])

    pygame.draw.rect(screen, SUN_YELLOW, [452-425+x, 185-185+y, 40, 20])


pygame.mouse.set_visible(0)
y_speed = 0
x_speed = 0
x_coord = 10
y_coord = 10


done = False

clock = pygame.time.Clock()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x_speed = -3
            elif event.key == pygame.K_RIGHT:
                x_speed = 3
            elif event.key == pygame.K_UP:
                y_speed = -3
            elif event.key == pygame.K_DOWN:
                y_speed = 3
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_speed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                y_speed = 0
    x_coord = x_coord + x_speed
    y_coord = y_coord + y_speed
    pos = pygame.mouse.get_pos()
    x = pos[0]
    y = pos[1]

    screen.fill(GRASS_GREEN)

    # Sky
    pygame.draw.rect(screen, SKY_BLUE, [0, 0, 700, 240])

    # Hill
    pygame.draw.ellipse(screen, GRASS_GREEN, [-5, 120, 500, 300])

    # Sun
    pygame.draw.ellipse(screen, SUN_YELLOW, [-14, -10, 85, 80])

    pygame.draw.line(screen, SUN_YELLOW, [0, 50], [10, 140], 5)

    pygame.draw.line(screen, SUN_YELLOW, [40, 65], [70, 140], 5)

    pygame.draw.line(screen, SUN_YELLOW, [60, 40], [120, 100], 6)

    pygame.draw.line(screen, SUN_YELLOW, [60, 20], [150, 30], 5)

    # Clouds
    pygame.draw.ellipse(screen, WHITE, [470, 20, 90, 80])

    pygame.draw.ellipse(screen, WHITE, [530, 35, 90, 80])

    pygame.draw.ellipse(screen, WHITE, [490, 60, 90, 80])
    person(screen, x_coord, y_coord)
    person(screen, x-425, y-212)
    person0(screen, 0, 0)
    pygame.display.flip()

    clock.tick(60)    
    

pygame.quit()
