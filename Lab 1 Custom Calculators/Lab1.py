# Gabe N.
# Fahrenheit to Celsius calculator

# Input
temp_fahrenheit = float(input("Enter Fahrenheit:"))

# Fahrenheit to Celsius Formula
temp_celsius = (temp_fahrenheit - 32) * 5 / 9

# Output
print("Fahrenheit to Celsius:", temp_celsius)


# Gabe N.
# Trapezoid calculator

# Input
trapezoid_height = float(input("Enter the height of the trapezoid:"))

# Input
trapezoid_length_base = float(input("Enter the length of the bottom base:"))

# Input
trapezoid_length_top = float(input("Enter the length of the top base:"))

# Trapezoid formula
trapezoid_area = 1 / 2 * (trapezoid_length_base + trapezoid_length_top) * trapezoid_height

# Output
print("The area of the trapezoid is:", trapezoid_area)


# Gabe N.
# Area of a circle

# Input
radius = float(input("Enter radius of the circle:"))

# Area formula
circle_area = 3.141592653 * (radius ** 2)

# Output
print("Area of the circle:", circle_area)
