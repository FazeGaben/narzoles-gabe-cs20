# Gabe N.
# Create a picture
# March 13

import pygame
import random
PI = 3.14159265
# Colours

BLACK = (0, 0, 0)
YELLOW = (248, 223, 20)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (9, 135, 245)
GREY = (159, 157, 155)
pygame.init()
size = (1080, 720)
screen = pygame.display.set_mode(size)
screen.fill(BLACK)
for i in range(100):
    x_offset = random.randrange(1080)
    y_offset = random.randrange(720)
    pygame.draw.ellipse(screen, WHITE, [20 + x_offset, 20 + y_offset, 7, 7])
pygame.display.set_caption("UFO")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
    # --- Game logic should go here

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.

    # --- Drawing code should go here

    # Ship
    pygame.draw.ellipse(screen, GREY, [355, 270, 350, 150])
    pygame.draw.ellipse(screen, YELLOW, [950, -10, 200, 200])

    # Glass window
    pygame.draw.arc(screen, GREY, [391, 200, 280, 190], 0, PI / 2,)
    pygame.draw.arc(screen, GREY, [391, 200, 280, 190], PI / 2, PI,)

    # Alien head
    pygame.draw.ellipse(screen, GREEN, [500, 210, 40, 60])
    pygame.draw.line(screen, BLACK, [510, 255], [530, 255], 2)

    # Alien eyes
    pygame.draw.ellipse(screen, RED, [515, 240, 7, 7])
    pygame.draw.ellipse(screen, RED, [525, 240, 7, 7])

    # Alien lights
    pygame.draw.circle(screen, RED, [490, 345], 20)
    pygame.draw.circle(screen, RED, [430, 345], 20)
    pygame.draw.circle(screen, RED, [580, 345], 20)
    pygame.draw.circle(screen, RED, [640, 345], 20)

    # Alien thrusters
    pygame.draw.rect(screen, GREY, [325, 323, 60, 30])
    pygame.draw.line(screen, RED, [1, 300], [335, 335], 28)
    pygame.draw.line(screen, RED, [1, 360], [335, 335], 28)
    pygame.draw.line(screen, RED, [1, 330], [335, 340], 28)

    # Text
    font = pygame.font.SysFont('Calibri', 25, True, False)
    text = font.render("We come in peace! -->", True, WHITE)
    screen.blit(text, [200, 250])

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
