import pygame
import random
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
block_list = pygame.sprite.Group()
sprites_list = pygame.sprite.Group()

class Player(pygame.sprite.Sprite):
    def __init__(self, colour, width, height, y, x):
        super().__init__()
        self.speed = 5
        self.width = width
        self.height = height
        self.colour = colour
        self.y = y
        self.image = pygame.Surface([width, height])
        self.image.fill(colour)
        self.x = x

        self.rect = self.image.get_rect()

    def position(self, x, y):
        self.rect.y = y
        self.rect.x = x

    def draw(self, screen):
        pygame.draw.rect(screen, self.colour, self.rect)


class Balls(pygame.sprite.Sprite):
    def __init__(self, ball_colour, ball_width, ball_height, ball_x, ball_y, ball_radius):
        super().__init__()
        self.ball_colour = ball_colour
        self.ball_width = ball_width
        self.ball_height = ball_height
        self.ball_x = ball_x
        self.ball_y = ball_y
        self.ball_cx = 5
        self.ball_cy = 5
        self.ball_radius = ball_radius

        self.image = pygame.Surface([ball_width, ball_height])
        self.image.fill(ball_colour)
        self.rect = self.image.get_rect()
        self.rect.x = 525
        self.rect.y = 500

    def draw_ball(self, screen):
        pygame.draw.circle(screen, self.ball_colour, [self.rect.x, self.rect.y], self.ball_radius, self.ball_width)

    def ball_position(self, ball_x, ball_y):
        self.ball_x = ball_x
        self.ball_y = ball_y

    def update(self, x_speed, y_speed):
        self.rect.x += x_speed
        self.rect.y += y_speed


class Rectangles(pygame.sprite.Sprite):

    def __init__(self, colour, rec_width, rec_height, rec_x, rec_y):
        super().__init__()
        self.width = rec_width
        self.height = rec_height
        self.colour = colour
        self.rec_x = rec_x
        self.rec_y = rec_y
        self.image = pygame.Surface([rec_width, rec_height])
        self.image.fill(colour)
        self.rect = self.image.get_rect()

    def draw(self, screen,):
        for row in range(10):
            for column in range(100):
                blocks = pygame.draw.rect(screen,
                                          WHITE,
                                          [(block_space + block_width) * column + block_space,
                                           (block_space + block_height) * row + block_space,
                                           block_width,
                                           block_height])




#class Blocker():

    #def __init__(self, colour, width, height, x, y):

        #self.image = pygame.Surface([width, height])
        #self.image.fill(GREEN)
        #self.rect = self.image.get_rect()

        #self.colour = colour
       # self.x = x
        #self.y = y
      #  self.x_speed = 0

   # def position(self, x, y):
    #    self.rect.x = x
    #    self.rect.y = y

   # def draw(self, screen, x):
   #     pygame.draw.rect(screen, self.colour, [455 + x, 455, 150, 5])
    #    self.x += self.x_speed

  #  def update_blocker(self):
   #     self.x += self.x_speed


def blocker(screen, x):
    pygame.draw.rect(screen, WHITE, [455 + x, 455, 150, 5])


pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()
pygame.mouse.set_visible(0)

player = Player(RED, 100, 10, 650, 525)
player.y = 650
player.x = 525
sprites_list.add(player)

ball = Balls(GREEN, 5, 5, 525, 500, 55)
ball.ball_x = 525
ball.ball_y = 500
ball.ball_radius = 10
sprites_list.add(ball)

block = Rectangles(RED, 50, 50, 50, 50)
block.rec_x = 100
block.rec_y = 100


x_coord_blocker = 455
x_speed_blocker = 0
x_offset = -10
y_offset = -10
block_position = []
# Loop for each row
block_space = 5
block_width = 20
block_height = 20

#blocker = Blocker(GREEN, 150, 5, 455, 455)

#blocker.x += blocker.x_speed
#player.position(Player.x, Player.y)

#ball.ball_position(Balls.ball_x, Balls.ball_y)
score = 0
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:

            if event.key == pygame.K_LEFT:
                x_speed_blocker = -3
            elif event.key == pygame.K_RIGHT:
                x_speed_blocker = 3

        elif event.type == pygame.KEYUP:

            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_speed = x_speed_blocker


    # --- Game logic should go here

    blocks_hit = pygame.sprite.spritecollide(ball, block_list, True)
    if block_list == True:
        block_list.remove(block)


    mouse_position = pygame.mouse.get_pos()
    player.position(mouse_position[0], player.y)
    print(mouse_position)
    ball.update(3, 3)

    x_coord_blocker = x_coord_blocker + x_speed_blocker

    for block in blocks_hit:
        score += 1
        print(score)
   # blocker.update_blocker()

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(BLACK)

    # --- Drawing code should go here
    block.draw(screen)
    sprites_list.draw(screen)
    # blocker.draw(screen, blocker.x)
    blocker(screen, x_coord_blocker)
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()