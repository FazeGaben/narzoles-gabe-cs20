import pygame
import random
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)


class Rectangle():
    def __init__(self, x, y, width, height, change_x, change_y, colour):
        self.colour = colour
        self.image = pygame.Surface([width, height])
        self.image.fill(colour)
        self.rect = self.image.get_rect()
        self.change_x = change_x
        self.change_y = change_y
        self.x = x
        self.y = y
        self.screen = screen
        self.width = width
        self.height = height

    def move(self):
        self.y += self.change_y
        self.x += self.change_x

    def draw(self, screen):
        pygame.draw.rect(screen, self.colour [self.x, self.y], self.rect)



class Ellipse(Rectangle):
    def draw_ellipse(self, screen):
        pygame.draw.ellipse(screen, self.colour, self.rect)




pygame.init()

# Set the width and height of the screen [width, height]
size = (700, 500)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

my_object = Rectangle()

my_object.change_x += my_object.x
my_object.change_y += my_object.y
# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here

    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here

    my_object.draw(screen)
    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()