# Gabe N.

# Computer science 20

# Lab 7


# Room list
room_list = []
# Rooms
room = ["You enter a cold room, dimly lit by candles. There is a door to the south.", None, None, 1,
        None]
room_list.append(room)

room = ["You enter an overgrown room, vines are protruding from the ceiling, you pick up a key on a vine. "
        "There is a door to the west", 0, None, None, 2]
room_list.append(room)

room = ["You enter a room filled with grass. You see a double doors to the south and a sliding door to the west."
        , None, 1, 5, 3]
room_list.append(room)

room = ["You enter a pitch black room ", 4, 2, None, None]
room_list.append(room)

room = ["You enter a small closet. You feel a key, you pick it up.", None, None, 3, None]
room_list.append(room)

room = ["You enter a closet. There is a hole to the south.", 2, None, 6, None]
room_list.append(room)

room = ["You enter a triangular room. There is a garage door to the east and a small crack on the wall to the west",
        5, 8, None, 7]
room_list.append(room)

room = ["You enter a tight crevasse, you found nothing.", None, None, 6, None]
room_list.append(room)

room = ["You escaped through the garage door with a key"]
room_list.append(room)

current_room = 0
# While loop
done = False
while not done:
    # Print current room description
    print(room_list[current_room][0])

    print("W. North")
    print("S. South")
    print("A. West")
    print("D. East")
    print("Q. Quit")
    input_1 = input("Where do you want to go?")

    # Quit
    if input_1.lower() == "q":
        done = True

        # Move south
    elif input_1.lower() == "s":
        next_room = room_list[current_room][3]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room
            print("You went south.")

            # Move north
    elif input_1.lower() == "w":
        next_room = room_list[current_room][1]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room
            print("You went north.")

            # Move east
    elif input_1.lower() == "d":
        next_room = room_list[current_room][2]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room
            print("You went east.")

            # Move west
    elif input_1.lower() == "a":
        next_room = room_list[current_room][4]
        if next_room is None:
            print("You can't go that way.")
        else:
            current_room = next_room
            print("You went west.")

            # Escape
    if current_room == 8:
        print("You escaped through the garage door with the key")
        done = True
