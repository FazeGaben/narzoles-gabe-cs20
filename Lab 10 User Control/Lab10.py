# Gabe N.
# Comp sci 20
# Lab 10 User Control

import pygame
import random
PI = 3.14159265

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
GREY = (159, 157, 155)
YELLOW = (248, 223, 20)

# Stars
star_list = []
number_star = 500
for i in range(number_star):
    x = random.randrange(0, 1280)
    y = random.randrange(0, 720)
    star_list.append([x, y])

pygame.init()

# Set the width and height of the screen [width, height]
size = (1280, 720)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")


# Alien drawing function
def alien_ship(screen, x, y):
    # Ship
    pygame.draw.ellipse(screen, GREY, [355+x, 270+y, 350, 150])

    # Glass window
    pygame.draw.arc(screen, GREY, [391+x, 200+y, 280, 190], 0, PI / 2,)
    pygame.draw.arc(screen, GREY, [391+x, 200+y, 280, 190], PI / 2, PI,)

    # Alien head
    pygame.draw.ellipse(screen, GREEN, [500+x, 210+y, 40, 60])
    pygame.draw.line(screen, BLACK, [510+x, 255+y], [530+x, 255+y], 2)

    # Alien eyes
    pygame.draw.ellipse(screen, RED, [515+x, 240+y, 7, 7])
    pygame.draw.ellipse(screen, RED, [525+x, 240+y, 7, 7])

    # Alien lights
    pygame.draw.circle(screen, RED, [490+x, 345+y], 20)
    pygame.draw.circle(screen, RED, [430+x, 345+y], 20)
    pygame.draw.circle(screen, RED, [580+x, 345+y], 20)
    pygame.draw.circle(screen, RED, [640+x, 345+y], 20)


# (0,0)
def alien_0(screen, x, y):
    # Ship
    pygame.draw.ellipse(screen, GREY, [355-355 + x, 270-200 + y, 350, 150])

    # Glass window
    pygame.draw.arc(screen, GREY, [391-355 + x, 200-200 + y, 280, 190], 0, PI / 2, )
    pygame.draw.arc(screen, GREY, [391-355 + x, 200-200 + y, 280, 190], PI / 2, PI, )

    # Alien head
    pygame.draw.ellipse(screen, GREEN, [500-355 + x, 210-200 + y, 40, 60])
    pygame.draw.line(screen, BLACK, [510-355 + x, 255-200 + y], [530-355 + x, 255-200 + y], 2)

    # Alien eyes
    pygame.draw.ellipse(screen, RED, [515-355 + x, 240-200 + y, 7, 7])
    pygame.draw.ellipse(screen, RED, [525-355 + x, 240-200 + y, 7, 7])

    # Alien lights
    pygame.draw.circle(screen, RED, [490-355 + x, 345-200 + y], 20)
    pygame.draw.circle(screen, RED, [430-355 + x, 345-200 + y], 20)
    pygame.draw.circle(screen, RED, [580-355 + x, 345-200 + y], 20)
    pygame.draw.circle(screen, RED, [640-355 + x, 345-200 + y], 20)
# Hidden mouse


pygame.mouse.set_visible(0)

y_speed = 0
x_speed = 0

# Keyboard current position
x_coord = 10
y_coord = 10

done = False


clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:

    # keyboard coord and speed
    x_coord = x_coord + x_speed
    y_coord = y_coord + y_speed
    pos = pygame.mouse.get_pos()

    # --- Main event loop
    # Keyboard inputs
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x_speed = -3
            elif event.key == pygame.K_RIGHT:
                x_speed = 3
            elif event.key == pygame.K_UP:
                y_speed = -3
            elif event.key == pygame.K_DOWN:
                y_speed = 3
        elif event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_speed = 0
            elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                y_speed = 0

        # Off-screen barriers
        if x_coord > 900:
            x_coord = x_coord * 0 + 898
        elif x_coord < 0:
            x_coord = x_coord * 0 + 1
        elif y_coord > 500:
            y_coord = y_coord * 0 + 495
        elif y_coord < 0:
            y_coord = y_coord * 0 + 1

    screen.fill(BLACK)

    # Stars
    for i in range(len(star_list)):
        pygame.draw.circle(screen, WHITE, star_list[i], 2)
        star_list[i][0] -= 20
        if star_list[i][0] < 0:

            x = random.randrange(1280, 1290)
            star_list[i][0] = x

    # Keyboard
    alien_0(screen, x_coord, y_coord)

    # Mouse
    alien_ship(screen, pos[0] - 425, pos[1] - 212)
    # (0,0)
    alien_0(screen, 0, 0)
    pygame.display.flip()

    clock.tick(60)

pygame.quit()
