import pygame


class GameObject():
    def __init__(self, colour, width, height):
        self.colour = colour
        self.image = pygame.Surface([width, height])
        self.image.fill(colour)

        self.rect = self.image.get_rect()

        self.vX = 0
        self.vY = 0

        self.alive = True

    def draw(self, screen):
        pygame.draw.rect(screen, self.colour, self.rect)

    def setVelocity(self, vX, vY):
        self.vX = vX
        self.vY = vY

    def update(self):
        self.rect.x += self.vX
        self.rect.y += self.vY

    def setPosition(self, x, y):
        self.rect.x = x
        self.rect.y = y

    def isCollide(self, target):
        if self.rect.colliderect(target.rect):
            return True

        return False
