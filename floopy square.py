import random
import pygame
from gameobject import *
import c
# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)

pygame.init()

# Set the width and height of the screen [width, height]
size = (c.screen_width, c.screen_height)
screen = pygame.display.set_mode(size)

pygame.display.set_caption("My Game")

# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
pipe_list = []
clock = pygame.time.Clock()
player = GameObject(BLACK, c.player_width, c.player_height)
player.setPosition(c.player_x, 100)
for i in range(c.number_pipes):

    c.screen_width = c.screen_width / 2

    gap = random.randrange(c.min_pipe_gap, c.max_pipe_gap + 1)

    top = random.randrange(50, c.screen_height - 100 - gap)

    width = random.randrange(c.min_pipe_width, c.max_pipe_width + 1)

    separation = random.randrange(c.min_pipe_separation, c.max_pipe_separation)

    top_pipe = GameObject(GREEN, width, top)

    top_pipe.setPosition(100, gap)

    bottom_pipe = GameObject(GREEN, width, c.screen_height)

    bottom_pipe.setPosition(0, c.screen_width)

    pipe_list.append([top_pipe, bottom_pipe])

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # --- Game logic should go here
    mouse_position = pygame.mouse.get_pos()
    player.setPosition(c.player_x, mouse_position[1])
    # --- Screen-clearing code goes here

    # Here, we clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.

    # If you want a background image, replace this clear with blit'ing the
    # background image.
    screen.fill(WHITE)

    # --- Drawing code should go here
    player.draw(screen)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
