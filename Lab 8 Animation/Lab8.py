# Gabe N.
# Animation
# April 15, 2019
# Computer Science 20

import pygame
import random
PI = 3.14159265
# Colours
lights_colour = (random.randrange(256), random.randrange(256), random.randrange(256))
star_list = []
number_star = 500
alien_eye = 1
BLACK = (0, 0, 0)
YELLOW = (248, 223, 20)
WHITE = (255, 255, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)
BLUE = (9, 135, 245)
GREY = (159, 157, 155)
pygame.init()
screen_height = 720
screen_width = 1280
size = (screen_width, screen_height)
screen = pygame.display.set_mode(size)
screen.fill(BLACK)

pygame.display.set_caption("UFO")
for i in range(number_star):
    x = random.randrange(0, 1280)
    y = random.randrange(0, 720)
    star_list.append([x, y])
sun_x = 950
# Loop until the user clicks the close button.
done = False

# Used to manage how fast the screen updates
clock = pygame.time.Clock()

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    screen.fill(BLACK)

    # Stars
    for i in range(len(star_list)):
        pygame.draw.circle(screen, WHITE, star_list[i], 2)
        star_list[i][0] -= 20
        if star_list[i][0] < 0:

            x = random.randrange(1280, 1290)
            star_list[i][0] = x
    # Ship
    pygame.draw.ellipse(screen, GREY, [355, 270, 350, 150])

    # Sun
    pygame.draw.ellipse(screen, YELLOW, [sun_x, -10, 200, 200])
    sun_x -= 1

    # Glass window
    pygame.draw.arc(screen, GREY, [391, 200, 280, 200], 0, PI / 2, )
    pygame.draw.arc(screen, GREY, [391, 200, 280, 200], PI / 2, PI, )

    # Alien head
    pygame.draw.ellipse(screen, GREEN, [500, 210, 40, 60])

    # Mouth
    pygame.draw.line(screen, BLACK, [510, 255], [530, 255], 2)

    # Alien eyes

    pygame.draw.ellipse(screen, BLACK, [515, 240, 7, 7])
    pygame.draw.ellipse(screen, BLACK, [525, 240, 7, 7])

    # Alien lights
    pygame.draw.circle(screen, lights_colour, [490, 345], 20)
    pygame.draw.circle(screen, lights_colour, [430, 345], 20)
    pygame.draw.circle(screen, lights_colour, [580, 345], 20)
    pygame.draw.circle(screen, lights_colour, [640, 345], 20)
    if lights_colour == lights_colour:
        lights_colour = (random.randrange(256), random.randrange(256), random.randrange(256))

    # Alien thrusters
    pygame.draw.rect(screen, GREY, [325, 323, 60, 30])
    pygame.draw.line(screen, RED, [1, 300], [335, 335], 28)
    pygame.draw.line(screen, RED, [1, 360], [335, 335], 28)
    pygame.draw.line(screen, RED, [1, 330], [335, 340], 28)

    # Text
    font = pygame.font.SysFont('Calibri', 25, True, False)
    text = font.render("We come in peace! -->", True, WHITE)
    screen.blit(text, [200, 250])

    pygame.display.flip()

    # --- Limit to 60 frames per second
    clock.tick(60)

# Close the window and quit.
pygame.quit()
